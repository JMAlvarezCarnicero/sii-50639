// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=1.0f;
	velocidad.x=3;
	velocidad.y=3;
	ti=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	
	centro.x=centro.x+velocidad.x*t;
	centro.y=centro.y+velocidad.y*t;
	
	ti++;
	if(ti%150==0 && radio>0.25)
	{
		radio-=0.25;
	}
		
	
}

void Esfera::setRadio(float r)
{
	radio=r;
}
