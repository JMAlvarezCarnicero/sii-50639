#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

 int main(int argc, char* argv[]) 
{
 	mkfifo("/tmp/fifologger",0777);
 	int fd=open("/tmp/fifologger", O_RDONLY);
	int ex=0;
	int aux;
	while(ex==0)
 	{
 		char buff[200];
		aux=read(fd,buff,sizeof(buff));
		if((aux==-1) /*|| (aux!=sizeof(buff))*/) //Si hay error en read
		{
			printf("Logger Cerrado. \n");
			ex=1; //salimos del bucle
		}
		printf("%s\n", buff);
 	}
 	close(fd);
	unlink("/tmp/fifologger");
	return 0;
}

