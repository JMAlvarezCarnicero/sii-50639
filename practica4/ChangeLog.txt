Archivo ChangeLog 

Practica 2: implementado el movimiento de esfera y raquetas.
	   Implementada funcionalidad de parar las raquetas, con las teclas 'q' o 'p'.
Practica3: Version del logger finalizada y funcionando correctamente.
	   Primera version del bot operativa.
	   Añadido el juego hasta 3 puntos. El que llegue a 3 gana.	

Funcionamiento bot: - El jugador 1 empieza como bot por defecto. Al tocar una tecla de accion ('q', 'w' o 's') ira a modo usuario, si pulsamos 'a' volvera a bot.
		- El jugador 2 empieza en modo usuario. Si no se pulsa tecla al pasar 10 segundos ira a modo bot. Si pulsamos una tecla de accion('p', 'o' o 'l') volvera a modo usuario. Despues de pulsar la tecla si en 5 segundos no se pulsa de nuevo volvera a modo bot.
	
