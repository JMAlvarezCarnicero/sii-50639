INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")
FIND_PACKAGE( Threads )

SET(COMMON_SRCS_S 
	MundoServidor.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
SET(COMMON_SRCS_C 
	MundoCliente.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
				
ADD_EXECUTABLE(cliente cliente.cpp ${COMMON_SRCS_C})
ADD_EXECUTABLE(servidor servidor.cpp ${COMMON_SRCS_S})
ADD_EXECUTABLE(logger logger.cpp)
ADD_EXECUTABLE(bot bot.cpp)

TARGET_LINK_LIBRARIES(cliente glut GL GLU)
TARGET_LINK_LIBRARIES(servidor glut GL GLU)
TARGET_LINK_LIBRARIES(cliente ${CMAKE_THREAD_LIBS_INIT})
TARGET_LINK_LIBRARIES(servidor ${CMAKE_THREAD_LIBS_INIT})
