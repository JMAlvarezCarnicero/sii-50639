// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
//Cierre de la tuberia del logger
	char cerrar[100];
	sprintf(cerrar,"Tenis cerrado.");
	write(fdlogger,cerrar,strlen(cerrar)+1);
	close(fdlogger);

//cierre tuberia c-s
	close(fdcs);

//cierre tuberia tecla
	close(fdtecla);
	
}


void* hilo_comandos(void* d)
{
     		CMundo* p=(CMundo*) d;
    		p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
	while (1) {
		usleep(10);
		char cad[10];
		int r=read(fdtecla, cad, sizeof(cad));
		if(r==-1){
			perror("error leer tecla");
			exit(0);
		}
		//std::cout<<cad<<"\n";
		unsigned char key;
		sscanf(cad,"%c",&key);
		if(key=='s')jugador1.velocidad.y=-2.5;
		if(key=='w')jugador1.velocidad.y=2.5;
		if(key=='q')jugador1.velocidad.y=0;
		if(key=='l')jugador2.velocidad.y=-3;
		if(key=='o')jugador2.velocidad.y=3;
		if(key=='p')jugador2.velocidad.y=0;
		if(key=='a')pulsar1=0;
      }
}


void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{		
	cont10--; //Cuenta 10 sec para activar el bot 2
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		esfera.setRadio(1);
		char buff[100];
		sprintf(buff,"Jugador 2 marca 1 punto, lleva %d puntos.", puntos2);
		write(fdlogger,buff,strlen(buff)+1);
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		esfera.setRadio(1);
		char buff[100];
		sprintf(buff,"Jugador 1 marca 1 punto, lleva %d puntos.", puntos1);
		write(fdlogger,buff,strlen(buff)+1);
	}

//Juego a 3 puntos y finalizacion
	if(puntos1==3)
	{
		usleep(25000);
		printf("Jugador 1 ha llegado a 3 puntos. Gana la partida.\n");
		exit(0);
	}

	if(puntos2==3)
	{
		usleep(25000);
		printf("Jugador 2 ha llegado a 3 puntos. Gana la partida.\n");
		exit(0);
	}

//Lectura y envio de datos tuberia c-s
	char cad[300];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x,esfera.centro.y,esfera.radio,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
	int w = write(fdcs,cad,strlen(cad)+1);
	if(w==-1){perror("error write coord");exit(0);}
}



void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	//enviamos las pulsaciones de tecla desde el cliente
	/*case 's':jugador1.velocidad.y=-4;pulsar1=1;break;
	case 'w':jugador1.velocidad.y=4;pulsar1=1;break;
	case 'q':jugador1.velocidad.y=0;pulsar1=1;break;
	case 'a':pulsar1=0;break;
	case 'l':jugador2.velocidad.y=-4;cont10=125;break;
	case 'o':jugador2.velocidad.y=4;cont10=125;break;
	case 'p':jugador2.velocidad.y=0;cont10=125;break;*/

	}
}


void CMundo::Init()
{
//inicializacion tuberia para el logger
	fdlogger = open("/tmp/fifologger", O_WRONLY);
    
//inicializacion variables de contador y comprobacion de tecla
	cont10=250;
	pulsar1=0;
	pulsar2=0; //pulsar2 no es utilizada, pero puede utilizarse al cambiar alguna funconalidad del bot de jugador2


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


//abrimos tuberia c-s
	fdcs=open("/tmp/fifocs",O_WRONLY);
	if(fdcs==-1)perror("error open c-s s");


//creacion thread y tuberia tecla
	pthread_create(&th,NULL,hilo_comandos,this);
	fdtecla = open("/tmp/fifotecla",O_RDONLY);
	if(fdtecla==-1)perror("error open tecla s");

}



