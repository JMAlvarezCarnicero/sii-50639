// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
//Cierre de la tuberia del logger
	char cerrar[100];
	sprintf(cerrar,"Tenis cerrado.");
	write(fdlogger,cerrar,strlen(cerrar)+1);
	close(fdlogger);
//Destruccion de la proyeccion en memoria
	munmap(pdatos,sizeof(datos));

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{		
	cont10--; //Cuenta 10 sec para activar el bot 2
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		esfera.setRadio(1);
		char buff[100];
		sprintf(buff,"Jugador 2 marca 1 punto, lleva %d puntos.", puntos2);
		write(fdlogger,buff,strlen(buff)+1);
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		esfera.setRadio(1);
		char buff[100];
		sprintf(buff,"Jugador 1 marca 1 punto, lleva %d puntos.", puntos1);
		write(fdlogger,buff,strlen(buff)+1);
	}

//Juego a 3 puntos y finalizacion
	if(puntos1==3)
	{
		usleep(25000);
		printf("Jugador 1 ha llegado a 3 puntos. Gana la partida.\n");
		exit(0);
	}

	if(puntos2==3)
	{
		usleep(25000);
		printf("Jugador 2 ha llegado a 3 puntos. Gana la partida.\n");
		exit(0);
	}

	

//actualizacion de datos para el bot
	pdatos->esfera=esfera;
	pdatos->raqueta1=jugador1;
	pdatos->raqueta2=jugador2;
	
	if(pulsar1==0)
	{
		switch(pdatos->accion1)
		{
		case 1: jugador1.velocidad.y=2.5;break;
		case -1: jugador1.velocidad.y=-2.5;break;
		case 0: jugador1.velocidad.y=0;break;
		}
	}


	if((cont10<0) /*&& (pulsar2!=1)*/)
	{
		switch(pdatos->accion2)
		{
		case 1: jugador2.velocidad.y=3;break;
		case -1: jugador2.velocidad.y=-3;break;
		case 0: jugador2.velocidad.y=0;break;
		}
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;pulsar1=1;break;
	case 'w':jugador1.velocidad.y=4;pulsar1=1;break;
	case 'q':jugador1.velocidad.y=0;pulsar1=1;break;
	case 'a':pulsar1=0;break;
	case 'l':jugador2.velocidad.y=-4;/*pulsar2=1;*/cont10=125;break;
	case 'o':jugador2.velocidad.y=4;/*pulsar2=1;*/cont10=125;break;
	case 'p':jugador2.velocidad.y=0;/*pulsar2=1;*/cont10=125;break;

	}
}

void CMundo::Init()
{
//inicializacion tuberia para el logger
	fdlogger = open("/tmp/fifologger", O_WRONLY);

//creacion de fichero y de la proyeccion en memoria para el bot
	datos.esfera = esfera;
	datos.raqueta1 = jugador1;
	datos.raqueta2 = jugador2;

	fdbot = open("/tmp/bot_file.txt",O_RDWR|O_CREAT|O_TRUNC,0777);
	if(fdbot==-1){printf("error open0");}
	int w = write(fdbot,&datos,sizeof(datos));
	if(w==-1){printf("error write");}
	pdatos = (DatosMemCompartida*)mmap(NULL,sizeof(datos),PROT_WRITE|PROT_READ,MAP_SHARED,fdbot,0);	//proyeccion en memoria del fichero
	close(fdbot);

	pdatos->accion1=0;  //valor inicial de accion (raqueta estatica) 
	pdatos->accion2=0;
    
//inicializacion variables de contador y comprobacion de tecla
	cont10=250;
	pulsar1=0;
	pulsar2=0; //pulsar2 no es utilizada, pero ude utilizarse al cambiar alguna funconalidad del bot de jugador2
	

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
